# express-graphql-typescript

Demo Express TypeScript server with a GraphQL runtime

## Build and run

To start locally Node is required. If you use nvm, simply issue `nvm use`.

Install the needed dependencies and put in place the .env file for local environment variables:

```bash
make init
```

Start the database with Docker:

```bash
make start-db
```

Start the backend with NPM:

```bash
make npm-start
```

The GraphQL API Playground is available at http://localhost:4000/graphql

## Example queries

```graphql
query GET {
  getTodos {
    title
    description,
    enabled
  }
}

mutation POST {
  addTodo(todoInput: { title: "test", description: "is fun"}) {
    id
    title,
    enabled
  }
}
```

## Resources

To get a Postgres shell:

```bash
docker-compose exec --env PGPASSWORD=password graphql-db psql --username=user --dbname=graphql
```

To get a backend shell:

```bash
docker-compose exec backend bash
```

To get backend Docker logs:

```bash
docker-compose logs -f backend
```
