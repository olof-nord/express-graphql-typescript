.SILENT:

.DEFAULT_GOAL := start

.PHONY : init build start stop purge npm-install npm-build npm-start

init: npm-install
	cp .env.development .env

## Docker ##

build:
	docker-compose build

start:
	docker-compose up --detach

start-db:
	docker-compose up --detach graphql-db

stop:
	docker-compose stop

purge:
	docker-compose down --remove-orphans --volumes

## NPM ##

npm-install:
	@echo "Installing NPM dependencies"
	npm install

npm-build:
	@echo "Building NPM package"
	npm run build

npm-start:
	@echo "Starting backend"
	npm run dev
