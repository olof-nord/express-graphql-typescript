import { Field, ObjectType, InputType, ID } from "type-graphql";
import { IsNotEmpty, MaxLength } from "class-validator";
import { Entity, PrimaryKey, Property, wrap } from "@mikro-orm/core";
import { v4 as uuidv4 } from "uuid";

@ObjectType()
@Entity()
export class Todo {
  constructor(params: Partial<Todo>) {
    wrap(this).assign(params);
  }

  @Field(() => ID)
  @PrimaryKey({ length: 36 })
  id: string = uuidv4()

  @Field()
  @Property()
  title!: string;

  @Field()
  @Property()
  description!: string;

  @Field()
  @Property({ default: true })
  enabled: boolean = true;
}

@InputType()
export class TodoInput implements Partial<Todo> {
  @IsNotEmpty()
  @MaxLength(255)
  @Field()
  title!: string;

  @IsNotEmpty()
  @MaxLength(255)
  @Field()
  description!: string;
}
