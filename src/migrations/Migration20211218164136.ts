import { Migration } from '@mikro-orm/migrations';

export class Migration20211218164136 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "todo" ("id" varchar(36) not null, "title" varchar(255) not null, "description" varchar(255) not null, "enabled" bool not null default true);');
    this.addSql('alter table "todo" add constraint "todo_pkey" primary key ("id");');
  }

  async down(): Promise<void> {
    this.addSql('drop table "todo";');
  }

}
