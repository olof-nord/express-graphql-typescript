import { buildSchema } from "type-graphql";
import express  from "express";
import { ApolloServer } from "apollo-server-express";
import { ApolloServerPluginLandingPageGraphQLPlayground as GraphQLPlayground } from "apollo-server-core";

import { initPostgres } from "./services/dbService";

import "reflect-metadata";

const PORT = process.env.PORT || 4000;

const startServer = async () => {

  const app: express.Application = express();

  const schema = await buildSchema({
    resolvers: [ __dirname + "/resolvers/**/*.{ts,js}"],
    validate: true,
  })

  const server = new ApolloServer({
    schema,
    plugins: [
      GraphQLPlayground()
    ]
  });

  await server.start()

  server.applyMiddleware({ app });

  await initPostgres();

  app.listen(PORT, () => {
    console.log(`🚀 Server ready at http://localhost:${PORT}`);
  });
};

startServer();
