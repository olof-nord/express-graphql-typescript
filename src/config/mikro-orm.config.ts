import { Options } from '@mikro-orm/core';

const options: Options = {
  type: "postgresql",
  entities: ["build/entities"],
  entitiesTs: ["src/entities"],
  debug: process.env.NODE_ENV === "development",
  migrations: {
    path: "src/migrations",
  },
};

export default options;
