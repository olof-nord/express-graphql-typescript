import { DI } from "./dbService";
import { Todo, TodoInput } from "../entities/todo";

const getAll = () => {
  return DI.todoRepository.findAll();
}

const getOne = async (id: string) => {
  return DI.todoRepository.findOne({
    id,
  });
}

const add = async (todoInput: TodoInput) => {
  const todo = new Todo({
    title: todoInput.title,
    description: todoInput.description,
  });

  await DI.todoRepository.persistAndFlush(todo);
  return todo;
}

export default {
  getAll,
  getOne,
  add
}
