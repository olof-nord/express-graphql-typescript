import { EntityManager, EntityRepository, MikroORM } from "@mikro-orm/core";

import { Todo } from "../entities/todo";

export const DI = {} as {
  orm: MikroORM,
  em: EntityManager,
  todoRepository: EntityRepository<Todo>,
};

export const initPostgres = async (): Promise<void> => {
  DI.orm = await MikroORM.init();

  DI.em = DI.orm.em;
  DI.todoRepository = DI.orm.em.getRepository(Todo);

}
