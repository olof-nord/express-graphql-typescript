import { Query, Resolver, Mutation, Arg } from "type-graphql";

import { Todo, TodoInput } from "../entities/todo";
import todoService from "../services/todoService";

@Resolver(Todo)
class TodoResolver {

  @Query(() => [Todo])
  async getTodos(): Promise<Todo[]> {
    return todoService.getAll();
  }

  @Mutation(() => Todo)
  async addTodo(
    @Arg("todoInput") todoInput: TodoInput
  ): Promise<Todo> {
    return todoService.add(todoInput);
  }
}

export default TodoResolver;
