FROM node:16.13-slim as build

ARG PORT=4000
ARG MIKRO_ORM_HOST=graphql-db
ARG MIKRO_ORM_USER=user
ARG MIKRO_ORM_PASSWORD=password
ARG MIKRO_ORM_DB_NAME=graphql

USER node

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY package.json package-lock.json ./
RUN npm ci

COPY . .
RUN npm run build

ENV TZ=Europe/Berlin

ENV PORT=${PORT}
ENV MIKRO_ORM_HOST=${MIKRO_ORM_HOST}
ENV MIKRO_ORM_USER=${MIKRO_ORM_USER}
ENV MIKRO_ORM_PASSWORD=${MIKRO_ORM_PASSWORD}
ENV MIKRO_ORM_DB_NAME=${MIKRO_ORM_DB_NAME}

EXPOSE ${PORT}

CMD [ "npm", "run", "start" ]
